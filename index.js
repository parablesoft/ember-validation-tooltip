/* jshint node: true */
'use strict';

module.exports = {
  name: 'ember-validation-tooltip',

	included: function(app){
		app.import("vendor/tooltip/tooltip.js");
		app.import("vendor/tooltip/tooltip.css");
	}
};

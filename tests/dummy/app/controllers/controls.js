import Ember from "ember";

const {set,Controller} = Ember;

export default Controller.extend({
	submitted: true,
	titleValidation: false,
	actions: {
		submit(){
			set(this,"submitted",true);
		}
	}
});

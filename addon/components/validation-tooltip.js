import Ember from 'ember';
const {computed,isPresent,set,get,observer,on,$} = Ember;
const {and,not,alias} = computed;

export default Ember.Component.extend({
	notValid: not("valid"),
	showValidation: and("submitted","notValid"),
	message: null,
	defaultMessage: "Please enter a value",
	validationMessage: computed("message","defaultMessage",function(){
		let validationMessage = get(this,"defaultMessage");
		if(get(this,"message") != null){
			validationMessage = get(this,"message");
		}
		return validationMessage;
	}),
	tooltip: null,
	tooltipPlacement: "right",
	setTooltipStyle: on("didInsertElement",function(){
		let tooltip = new Tooltip(get(this,"validationMessage"),{
			place: get(this,"tooltipPlacement"),
			typeClass: "error",
			effectClass: "fade",
		});
		set(this,"tooltip",tooltip);
		Ember.run.later(this,()=>{
		let submitted = get(this,"submitted");
		if(submitted){
			this.displayTooltip();
		}},250);
	}),
	destroyTooltip: on("willDestroyElement",function(){
		get(this,"tooltip").detach();
		set(this,"tooltip",null);
	}),
	inputSelector: null,
	inputFinder:null,//siblings or parent
	parentId: alias("parentView.elementId"),
	parentSelector: computed("parentId",function(){
		let parentId = get(this,"parentId");
		return $(`#${parentId}`);
	}),
	targetSelector: computed("inputSelector","parentId",function(){
		let inputSelector = get(this,"inputSelector");
		let parentSelector = get(this,"parentSelector");
		if(inputSelector==null){
			return parentSelector;
		}
		let inputFinder = get(this,"inputFinder");
		if(inputFinder=="siblings"){
			return parentSelector.siblings(`.${inputSelector}`);
		}
		else{
			return parentSelector.closest("div");//(`.${inputSelector}`);
		}
	}),
	showMessage: observer("submitted","valid",function(){
		this.displayTooltip();
	}),
	displayTooltip(){
		let showValidation = get(this,"showValidation");
		let tooltip = get(this,"tooltip");
		let targetSelector = get(this,"targetSelector");
		tooltip.position(targetSelector[0]);
		showValidation ? tooltip.show() : tooltip.hide();
		get(this,"targetSelector").toggleClass("error",showValidation);
	}
});
